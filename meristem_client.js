//
//    Meristem, a minimal blockchain system for research
//
//    meristem_client.js: TODO
//
//  Copyright (c) 2018 Digisoft.com.uy
//  Copyright (c) 2018 Adrian Custer <acuster@gnome.org>
//
//  License GPLv3

const CRYPT = require('./crypto_primitives.js');
const MSTEM = require('./lib_meristem.js');


class BlkchNode {
    constructor(){
        this.pendingTx = [];
        this.blockchain = new MSTEM.Blockchain();
    }

    pushBlock(BlockString){
        //Deserialize
        const blk = JSON.parse(BlockString);

        //Validate Block
        
    }

    generateGenesisBlock(){
        if ( 0 < this.blockchain.length ){
            //log error
            return; //there is alread a genesis block
        }
        this.blockchain.push( new MSTEM.Block(null, null) );
    }

    //TODO: every 20 seconds, if tx, generate new Block
    generateBlock(){
        if ( 0 === this.pendingTx.length ){
            return; //nothing to do
        }
        //TODO: could add own Tx: miner reward
        const txObj = {};
        this.pendingTx.forEach( function(tx,indx,arr){
            const txhash = CRYPT.txHash(tx);
            txObj[txhash] = tx;//TODO: or JSON.stringify(tx) is this already a string?
        });
        const newBlock = new MSTEM.Block(this.blockchain.last(), txObj);
        this.blockchain.push(newBlock);
        //TODO: transmit to network
    }


    static validateBlock( blk ){

        const header = blk.header();
        if (    undefined === header
             || undefined === header.index
             || undefined === header.index
             || undefined === header.index
             || undefined === header.index ){

        }
    }



    

}
exports.BlkchNode = BlkchNode;


class MsgServer extends BlkchNode {

    msgSubmitTx( tx ){
        //Deserialize tx
        const txobj = JSON.parse(tx);
        //Validate tx
        //if ( !tx.validate() ){ return 'false' };

        this.pendingTx.push(tx);
        return 'true';
    }

    msgRequestBlockchain(){
        return JSON.stringify( this.blockchain );
    }

    msgRequestBlock( index ){
        const indx = JSON.parse(index);
        if (indx > this.blockchain.length){
            return 'false';
        }
        return JSON.stringify( this.blockchain.getBlock(indx) );
    }

    msgPropagateBlock( blk ){
        //if we have a blk at that id:                    cf it and log any conflicts
        //if we don't have the id and it's the next one:  validate and integrate, purge Tx.
        //if we don't have the id and it's in the future: request blocks from network
    }

}
exports.MsgServer = MsgServer;
