//
//    Meristem, a minimal blockchain system for research
//
//    meristem_node.js: a node on the blockchain network
//
//  Copyright (c) 2018 Digisoft.com.uy
//  Copyright (c) 2018 Adrian Custer <acuster@gnome.org>
//
//  License GPLv3

const libM = require('./lib_meristem.js');


////////////////////////////////////////////////////////////////////////////////
//  BlkchNode: A generic node for the Meristem blockchain system
//
//  The node accumulates transaction requests and, occaisionally, generates
//  a new block on the blockchain.
//
//  Classes extend this class to provide ways to receive transaction requests
//  through some specific mechanism.
//
////////////////////////////////////////////////////////////////////////////////
class BlkchNode {

    constructor(){
        this.pendingTx = [];
        this.blockchain = new libM.Blockchain();
    }

    pushBlock(BlockString){
        //Deserialize
        const blk = JSON.parse(BlockString);

        //Validate Block
        
    }

    generateGenesisBlock(){
        if ( 0 < this.blockchain.length ){
            //log error
            return; //there is alread a genesis block
        }
        this.blockchain.push( new libM.Block(null, null) );
    }

    //TODO: every 20 seconds, if tx, generate new Block
    generateBlock(){
        if ( 0 === this.pendingTx.length ){
            return; //nothing to do
        }

        // TRANSACTIONS OBJECT
        //TODO: could add own Tx: miner reward
        const txObj = {};
        //Sync
        const arr = this.pendingTx;
        for(var i =0; i < arr.length; i++){
            const tx = arr[i];
            txObj[tx.hash()] = tx.tx;
        }
        //Async does not work, ACTUALLY it does, bug was not with this.
        //this.pendingTx.forEach( function(tx,indx,arr){
        //    txObj[tx.hash()] = tx.tx;//TODO: or JSON.stringify(tx) is this already a string?
        //});
        this.pendingTx = [];
        
        const newBlock = new libM.Block(this.blockchain.last(), txObj);
        this.blockchain.push(newBlock);
        //TODO: transmit to network
    }


    static validateBlock( blk ){
        const header  = blk.header();
        const content = blk.content();
        if (    undefined === header
             || undefined === header.index
             || undefined === header.timestamp
             || undefined === header.prevHash
             || undefined === header.contentHash 
             || undefined === content ){
            return false;
        }
        
    }

}
exports.BlkchNode = BlkchNode;




////////////////////////////////////////////////////////////////////////////////
//  MsgServer, a node of the Meristem blockchain system which receives messages
//  as regular Javascript calls.
////////////////////////////////////////////////////////////////////////////////
class MsgServer extends BlkchNode {

    msgSubmitTx( txstr ){
        //Deserialize tx
        //const txobj = JSON.parse(txstr);
        //Validate tx
        //if ( !tx.validate() ){ return 'false' };
        const tx = libM.Transaction.fromString(txstr);
        this.pendingTx.push(tx);
        return 'true';
    }

    msgRequestBlockchain(){
        return JSON.stringify( this.blockchain );
    }

    msgRequestBlock( index ){
        const indx = JSON.parse(index);
        if (indx > this.blockchain.length){
            return 'false';
        }
        return JSON.stringify( this.blockchain.getBlock(indx) );
    }

    msgPropagateBlock( blk ){
        //if we have a blk at that id:                    cf it and log any conflicts
        //if we don't have the id and it's the next one:  validate and integrate, purge Tx.
        //if we don't have the id and it's in the future: request blocks from network
    }

}
exports.MsgServer = MsgServer;
