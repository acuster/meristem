//
//    Meristem, a minimal blockchain system for research
//
//    lib_meristem.js: Objects for the blockchain:
//      * Block
//      * Blockchain
//      * Transaction
//
//  Copyright (c) 2018 Digisoft.com.uy
//  Copyright (c) 2018 Adrian Custer <acuster@gnome.org>
//
//  License GPLv3

const crypto = require('crypto');


////////////////////////////////////////////////////////////////////////////////
//   Block, a wrapper for the block data structure
////////////////////////////////////////////////////////////////////////////////
// A Block data structure conceptually contains:
//   * a header, which contains a 'hash' of the content
//   * content, of key-value pairs { hash_of_element, 'transaction' element}
// with the 'hash' of the block calculated only on the header.
//
//
////////////////////////////////////////////////////////////////////////////////

//TODO: Generate this with the necessary functions:
//  * headerHasher
//  * contentHasher
//TODO: FIXME
// make properties strings so there is no overflow on numbers
class Block {

    constructor ( prevBlock, content ) {
        //Simple validation
        if ( undefined === prevBlock || undefined === content ){
            throw new Error("Block constructor needs to have 2 parameters (but may be null)");
        }

        //// Defined here so properties are in this order
        //Header
        this.index = null;
        this.timestamp = null;
        this.prevHash = null;
        this.contentHash = null;
        //Body
        this.content = null;

        if ( null === prevBlock){
            //Make a Genesis block
            this.index = 0;
            this.timestamp = Date.now();
            this.prevHash = Block.headerHasher(null);

            if ( null !== content ) {
                this.content = content;
            } else {
                this.content =  Block.genesisContent();
            }
            this.contentHash = Block.contentHasher( this.content );
        } else {
            //TODO: validate previous block
            //TODO: check content is non-null
            if ( null === content) { throw new Error("Block constructor cannot have a previous block but null content") };

            this.index = prevBlock.index + 1;//TODO: overflow
            this.timestamp = Date.now();
            this.prevHash = Block.headerHasher( prevBlock.header);
            this.contentHash = Block.contentHasher( content );
            this.content = content;
        }
    }

    //TODO: return as copies, not raw!
    get header() {
        return { prevHash: this.prevHash, timestamp: this.timestamp, index: this.index, contentHash: this.contentHash };
    }
    get body () {
        return { content: this.content};
    }



    static genesisContent(){ //TODO: this is a Tx_genesis

        //Nonce ensures each genesis block is unique
        const SHA256 = crypto.createHash('sha256');
        SHA256.update( crypto.randomBytes(256) );
        const nonce = SHA256.digest('hex');

        return "Genesis block " + nonce;
    }

    static headerHasher  (header){
        //TODO: check arg
        if ( null === header){
            return 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855';//sha256 of an empty string
        }
        const SHA256 = crypto.createHash('sha256');
        SHA256.update( JSON.stringify(header) );
        return SHA256.digest('hex');
    }
    static contentHasher (content){
        const SHA256 = crypto.createHash('sha256');
        SHA256.update( JSON.stringify(content) );
        return SHA256.digest('hex');
    }

   static contentValidator( content ){
        //Content is an object 'hash':Transaction
        //Content must now be a set of transactions, each can be validated by type, the object must have a hash of each and nothing else
        const transHashes = Object.keys(content);
        const transactions = Object.values(content);
        transactions.forEach( function(tx,indx,arr){
            const txObj = Transaction.fromObj(tx);
            if ( !txObj.validate() ) { return false; }
            const txhash = txObj.hash();
            const txhashindx = transHashes.indexOf(txhash);
            if ( -1 === txhashindx ){return false;}
            transHashes.splice( txhashindx, 1);
        });
        if ( 0 !== transHashes.length ) { return false;}

        return true;
   }
}
exports.Block = Block;


////////////////////////////////////////////////////////////////////////////////
//   Blockchain
////////////////////////////////////////////////////////////////////////////////
// A Blockchain data structure conceptually contains an ordered list of Blocks.
//
//
////////////////////////////////////////////////////////////////////////////////
//TODO: ? extend Array directly?
class Blockchain {
    constructor(/* TODO: build it with desired hash and validate functions */){
        this._chain = [];
    }
    push(block){
        //TODO: validate it is a block ?instanceof?
        this._chain.push(block);
    }
    length(){
        return this._chain.length;
    }
    getBlock( i ){
        return this._chain[i];
    }
    last(){
        return this._chain[this._chain.length-1];
    }
    validate(){
        //Walk the chain, validate each block, validate block.prevHash === hash( index -1);
        return this._chain.every( function(blk, index, arr){
            //Genesis block is different
            if ( 0 === index){
                //only validate contentHash
                if ( blk.contentHash !== Block.contentHasher( blk.body.content )  ) { return false; }
            } else {
                if ( ! Block.contentValidator( blk.body.content )  ) { return false; }
                if ( blk.contentHash !== Block.contentHasher( blk.body.content )  ) { return false; }
                if ( blk.prevHash !== Block.headerHasher( arr[index-1].header )  ) { return false; }
                //TODO: check index is one greater than previous
                //TODO: check timestamp is greater than previous
            }
            return true;
        });
    }

    //// UTILITY METHODS: to show the blockchain to users.

    validateInDetail(){
        const strings = [];
        const PAD_SIZE = String(this._chain.length).length;
        //Walk the chain, validate each block, validate block.prevHash === hash( index -1);
        this._chain.forEach( function(blk, index, arr){
            //Genesis block is different
            if ( 0 === index){
                //only validate contentHash
                if ( blk.contentHash !== Block.contentHasher( blk.body.content )  ) { strings.push("Genesis block content hash invalid"); }
            } else {
                if ( ! Block.contentValidator( blk.body.content )  )                { strings.push("Block "+ String(index).padStart(PAD_SIZE) + " content invalid"); }
                if ( blk.contentHash !== Block.contentHasher( blk.body.content )  ) { strings.push("Block "+ String(index).padStart(PAD_SIZE) + " content hash invalid"); }
                if ( blk.prevHash !== Block.headerHasher( arr[index-1].header )  )  { strings.push("Block "+ String(index).padStart(PAD_SIZE) + " previous hash invalid"); }
                //TODO: check index is one greater than previous
                //TODO: check timestamp is greater than previous
            }
        });
        if ( 0 === strings.length ){
            strings.push("Blockchain valid!");
        }
        return strings.join('\n');
    }

    showChainedHashes(){
        const strings = [];
        this._chain.forEach( function(b, index, arr){
            if ( 0 === index ){
                strings.push( "Genesis block:  " + Block.headerHasher( b.header )  );
            } else {
                strings.push( '-' );
                strings.push( "Previous block: " + b.header.prevHash );
                strings.push( "Block " + String(index).padStart(8) + ': ' + Block.headerHasher( b.header )  );
            }
        });
        return strings.join('\n');
    }
}
exports.Blockchain = Blockchain;


////////////////////////////////////////////////////////////////////////////////
// Transactions
////////////////////////////////////////////////////////////////////////////////

class Transaction {
    //this.known_types = {};

    constructor (){
        // The .tx property has the actual key:value pairs of the raw transaction
        this.tx = {};//TODO: could be a map
    }

    hash(){
        const HASHER = crypto.createHash('sha256');
        HASHER.update( JSON.stringify(this.tx) );
        return HASHER.digest('hex');
    }

    static fromString( str ){
        const o = JSON.parse(str);//TODO: catch parsing exception
        return Transaction.fromObj( o );
    }
    static fromObj( o ){
        const type = o.tx_type;
        //switch on type to instantiate
        const TransactionType = Transaction.known_types[type];
        if ( undefined === TransactionType ){ return null; };
        const detx = new TransactionType();
        for (var prop in o){
            detx.tx[prop] = o[prop];
        }
        return detx;
    }
}
Transaction.known_types = {};
exports.Transaction = Transaction;


class TransactionNote extends Transaction {
    constructor(){
        super();
        this.tx.tx_type = 'note';
    }

    validate(){
        if ( 'note' !== this.tx.tx_type ){ return false; }
        if ( undefined === this.tx.tx_content ){ return false; } //tx_content can be '' but not undefined
        return true;
    }
}
Transaction.known_types["note"] = TransactionNote;
exports.TransactionNote = TransactionNote;



