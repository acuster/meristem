//
//    Meristem, a minimal blockchain system for research
//
//    index.js: a driver using lib_meristem.js
//
//  Copyright (c) 2018 Digisoft.com.uy
//  Copyright (c) 2018 Adrian Custer <acuster@gnome.org>
//
//  License GPLv3


const libBC = require('./lib_meristem.js');


//// AT LEVEL OF CODE
/*



const dechain = new Blkch.Blockchain();

dechain.push( new Blkch.Block(null,null) );
dechain.push( new Blkch.Block(dechain._chain[dechain._chain.length-1], 'some content')  );
dechain.push( new Blkch.Block(dechain._chain[dechain._chain.length-1], 'some more content')  );
dechain.push( new Blkch.Block(dechain._chain[dechain._chain.length-1], 'even more content')  );



console.log( dechain.showChainedHashes() );
*/


//// AT LEVEL OF CLIENT

///Setup a network

const nodecode = require('./meristem_node.js');


const NODE_ONE = new nodecode.MsgServer();
NODE_ONE.generateGenesisBlock();

NODE_ONE.msgSubmitTx(`{"tx_type":"note","tx_content":"This is a start"}`);
NODE_ONE.msgSubmitTx(`{"tx_type":"note","tx_content":"This is some more"}`);

NODE_ONE.generateBlock();


NODE_ONE.msgSubmitTx(`{"tx_type":"note","tx_content":"This is to continue"}`);
NODE_ONE.msgSubmitTx(`{"tx_type":"note","tx_content":"This contines some more"}`);

NODE_ONE.generateBlock();


const bc = JSON.parse( NODE_ONE.msgRequestBlockchain() );
console.log(bc);

console.log( NODE_ONE.blockchain.showChainedHashes() );


/// Testing Transactions
/*
const lib_blkch = require('./lib_meristem.js');

const tx_str = `{"type":"note","content":"some string"}`;

lib_blkch.Transaction.fromString( tx_str );
*/

